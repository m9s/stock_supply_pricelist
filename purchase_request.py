#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.wizard import Wizard
from trytond.transaction import Transaction
from trytond.pool import Pool


class CreatePurchase(Wizard):
    'Create Purchase'
    _name = 'purchase.request.create_purchase'

    def __init__(self):
        super(CreatePurchase, self).__init__()
        self._error_messages.update({
            'party_no_pricelist': 'For this supplier is no pricelist selected! '
                                  'Please set a purchase pricelist for the '
                                  'supplier.',
            })

    def _create_purchase(self, data):
        pool = Pool()
        request_obj = pool.get('purchase.request')
        party_obj = pool.get('party.party')
        purchase_obj = pool.get('purchase.purchase')
        product_obj = pool.get('product.product')
        line_obj = pool.get('purchase.line')
        date_obj = pool.get('ir.date')

        form = data['form']
        if form.get('product') and form.get('party') and \
                form.get('company'):
            req_ids = request_obj.search([
                            ('id', 'in', data['ids']),
                            ('party', '=', False),
                        ])
            if req_ids:
                request_obj.write(req_ids, {
                            'party': form['party'],
                        })

        elif form.get('payment_term') and form.get('party') and \
                form.get('company'):
            local_context = {}
            local_context['company'] = form['company']
            with Transaction().set_context(**local_context):
                party_obj.write(form['party'], {
                            'supplier_payment_term': form['payment_term'],
                        })


        req_ids = request_obj.search([
                    ('id', 'in', data['ids']),
                    ('purchase_line', '=', False),
                    ('party', '=', False),
                ])
        if req_ids:
            return 'ask_user_party'

        today = date_obj.today()
        requests = request_obj.browse(data['ids'])
        purchases = {}
        # collect data
        for request in requests:
            if request.purchase_line:
                continue

            if not request.party.supplier_payment_term:
                return 'ask_user_term'

            key = (request.party.id, request.company.id, request.warehouse.id)
            if key not in purchases:
                if request.purchase_date and request.purchase_date >= today:
                    purchase_date = request.purchase_date
                else:
                    purchase_date = today
                purchase = {
                    'company': request.company.id,
                    'party': request.party.id,
                    'purchase_date': purchase_date,
                    'payment_term': request.party.supplier_payment_term.id,
                    'warehouse': request.warehouse.id,
                    'invoice_address': party_obj.address_get(request.party.id,
                        type='invoice'),
                    'lines': [],
                    }

                if request.party.pricelist_purchase:
                    purchase.update({
                             'currency': request.company.currency.id,
                             'pricelist': request.party.pricelist_purchase.id
                             })
                else:
                    self.raise_user_error('party_no_pricelist')

                purchases[key] = purchase
            else:
                purchase = purchases[key]

            line = self.compute_purchase_line(request)
            purchase['lines'].append(line)
            if request.purchase_date:
                if purchase.get('purchase_date'):
                    purchase['purchase_date'] = min(purchase['purchase_date'],
                                                    request.purchase_date)
                else:
                    purchase['purchase_date'] = request.purchase_date

        # Create all
        context = {}
        context['user'] = Transaction().user
        for purchase in purchases.itervalues():
            lines = purchase.pop('lines')
            with Transaction().set_context(**context):
                with Transaction().set_user(0):
                    purchase_id = purchase_obj.create(purchase)
            for line in lines:
                request_id = line.pop('request')
                line['purchase'] = purchase_id
                with Transaction().set_context(**context):
                    with Transaction().set_user(0):
                        line_id = line_obj.create(line)
                request_obj.write(request_id, {
                            'purchase_line': line_id,
                        })

        return 'end'

    def compute_purchase_line(self, request):
        pool = Pool()
        party_obj = pool.get('party.party')
        product_obj = pool.get('product.product')
        tax_rule_obj = pool.get('account.tax.rule')
        pricelist_obj = pool.get('pricelist.pricelist')

        line = {
            'product': request.product.id,
            'unit': request.uom.id,
            'quantity': request.quantity,
            'request': request.id,
            'description': request.product.name,
            }

        context = {}
        context['uom'] = request.uom.id
        context['supplier'] = request.party.id
        if request.party.pricelist_purchase:
            context['currency'] = request.party.pricelist_purchase.currency.id
            context['pricelist'] = request.party.pricelist_purchase.id

        # XXX purchase with several lines of the same product
        with Transaction().set_context(**context):
            price = pricelist_obj.get_price([request.product.id],
                    request.quantity)[request.product.id]

        if not price:
            self.raise_user_error('missing_price',
                (request.product.name, request.product.id), 'please_update')
        line['unit_price'] = price

        taxes = []
        for tax in request.product.supplier_taxes_used:
            if request.party and request.party.supplier_tax_rule:
                pattern = self._get_tax_rule_pattern(request)
                tax_id = tax_rule_obj.apply(request.party.supplier_tax_rule,
                        tax, pattern)
                if tax_id:
                    taxes.append(tax_id)
                continue
            taxes.append(tax.id)
        line['taxes'] = [('add', taxes)]
        return line

CreatePurchase()
