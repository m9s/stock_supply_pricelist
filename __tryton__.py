#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Stock Supply Pricelist',
    'name_de_DE': 'Bestellwesen Preislisten',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz/',
    'description': '''
    - Use of pricelists in module stock_supply
''',
    'description_de_DE': '''
    - Ermöglicht die Verwendung von Preislisten im Modul stock_supply
''',
    'depends': [
        'stock_supply',
        'account_invoice_pricelist',
        'purchase_pricelist',
    ],
    'xml': [
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
